import React, { useState } from 'react';


export const MainContext = React.createContext({})

  // @ts-ignore
const MainContextProvider = ({children}) => {
  const [mainData, updateMainData] = useState({
    selectedType: 1
  })
  // @ts-ignore
  const switchType = (data) => {
    updateMainData({...mainData, selectedType: data})
  }

  const values = {
    mainData,
    switchType
  }
  return <MainContext.Provider value={values}>
    {children}
  </MainContext.Provider>
}

export default MainContextProvider

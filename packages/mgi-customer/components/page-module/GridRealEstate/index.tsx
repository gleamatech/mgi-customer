import React from 'react'
import { ItemRealEstate } from 'ui-components'
import { Row, Col } from 'antd';



type GridRealEstateProps = {
  props?: any
    
}
const arr = [1,2,3,4,5,6,7,8,9,10, 11,12,13,14,15,16,17,18,19,20]
const GridRealEstate = ({props}: GridRealEstateProps) => {
    return(
            <div className="grid-real-estate-component">
                <Row gutter={[{xs:9,sm:15 }, {xs:16,sm:32}]} >
                {arr.map(item =>(
                    <Col xs={24} sm={12} lg={8} xl={6} xxl={4}  key={item}>
                         <ItemRealEstate />
                    </Col>
                ))}
                </Row>
                
               
            </div>
    )
}

export default GridRealEstate
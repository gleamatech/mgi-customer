import React from 'react'
import { ItemAgency } from 'ui-components'
import { Row, Col } from 'antd';



type GridAgencyProps = {
    props?: any

}
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
const GridAgency = (props: GridAgencyProps) => {
    return (
        <div className="grid-agency-component">
            <Row gutter={[{ xs: 9, sm: 15 }, { xs: 16, sm: 32 }]} >
                {arr.map(item => (
                    <Col xs={12} sm={12} md={6} xl={4} xxl={3} key={item}>
                        <ItemAgency item={item} />
                    </Col>
                ))}
            </Row>


        </div>
    )
}

export default GridAgency
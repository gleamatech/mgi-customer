import React, { useEffect, useRef, useState } from "react";
import styles from "../../styles/container/carousel.module.scss";

type ImageType = { id: number; url: string };

const ImageCarousel: React.FC<{ images?: ImageType[] }> = ({ images }) => {
  const [selectedImageIndex, setSelectedImageIndex] = useState(0);
  const [selectedImage, setSelectedImage] = useState<ImageType>();
  const carouselItemsRef = useRef<HTMLDivElement[] | null[]>([]);
  useEffect(() => {
    if (images && images[0]) {
      carouselItemsRef.current = carouselItemsRef.current.slice(
        0,
        images.length
      );
      setSelectedImageIndex(0);
      setSelectedImage(images[0]);  
    }
  }, [images]);

  const handleSelectedImageChange = (newIdx: number) => {
    if (images && images.length > 0) {
      setSelectedImage(images[newIdx]);
      setSelectedImageIndex(newIdx);  
      console.log("carouselItemsRef", carouselItemsRef);
      if (carouselItemsRef?.current[newIdx]) {
        carouselItemsRef?.current[newIdx]?.scrollIntoView({
          inline: "center",
          behavior: "smooth",
        });
      }
    }
  };

  return (
    <div className={styles.carousel__container}>
      <div
        className={styles.selected__image}
        style={{ backgroundImage: `url(${selectedImage?.url})` }}
      />
      <div className={styles.wrap__carousel}>
      <div className={styles.carousel}>
        <div className={styles.carousel__images}>
          {images &&
            images.map((image, idx) => (
              <div
                onClick={() => handleSelectedImageChange(idx)}
                style={{ backgroundImage: `url(${image.url})` }}
                key={image.id}
                className={`${styles.carousel__image} ${
                  selectedImageIndex === idx && "carousel__image-selected"
                }`}
                ref={(el) => (carouselItemsRef.current[idx] = el)}
              />
            ))}
        </div>
      </div>
      </div>
    </div>
  );
};

export default ImageCarousel;

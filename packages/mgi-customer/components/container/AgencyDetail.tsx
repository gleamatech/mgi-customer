import React, { useState } from "react";
import styles from "../../styles/container/agencyDetail.module.scss";
import Carousel from "../container/Carousel";
import cn from "classnames";
import Image from "next/image";

import { Table } from "ui-components";
import { Icon } from "ui-components";
import { Row, Col } from "antd";
import { ColumnsType } from "antd/es/table";

export interface ObjectDataSource {
  key?: string;
  name?: string;
  price?: string;
  image?: string;
}
export interface AgencyObject {
  dataIndex?: number;
  key?: number;
  title?: string;
  name?: string;
  render?: JSX.Element;
}

const dataSource: ObjectDataSource[] = [
  {
    key: "1",
    name: "A0272938",
    price: "12 Tỷ",
    image: "https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg",
  },
  {
    key: "1",
    name: "A0272938",
    price: "12 Tỷ",
    image: "https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg",
  },
  {
    key: "1",
    name: "A0272938",
    price: "12 Tỷ",
    image: "https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg",
  },
  {
    key: "1",
    name: "A0272938",
    price: "12 Tỷ",
    image: "https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg",
  },
  {
    key: "1",
    name: "A0272938",
    price: "12 Tỷ",
    image: "https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg",
  },
  {
    key: "1",
    name: "A0272938",
    price: "12 Tỷ",
    image: "https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg",
  },
];

const columns: ColumnsType<ObjectDataSource> = [
  {
    title: "Image",
    dataIndex: "image",
    key: "image",
    render: function renderImage(image: string): JSX.Element {
      return (
        <div
          className={styles.main_image}
          style={{ backgroundImage: `url(${image})` }}
        ></div>
      );
    },
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    // @ts-ignore
    render: function renderName(name: string): JSX.Element {
      return (
        <div className={styles.wrap__name}>
          <div className={styles.table__name}>{name}</div>
        </div>
      );
    },
  },
  {
    title: "detail",
    dataIndex: "detail",
    key: "detail",
    // @ts-ignore
    render: function renderDetail(detail): JSX.Element {
      return (
        <div className={styles.wrap__detail}>
          <div className={styles.detail__item}>
            <Icon type="Bold" name="StackSimple" />
            <div>300m2</div>
          </div>
          <div className={styles.detail__item}>
            <Icon type="Bold" name="Bed" />
            <div>6</div>
          </div>
          <div className={styles.detail__item}>
            <Icon type="Bold" name="Drop" />
            <div>4</div>
          </div>
          <div className={styles.detail__item}>
            <Icon type="Bold" name="Compass" />
            <div>Đông Nam</div>
          </div>
        </div>
      );
    },
  },
  {
    title: "price",
    dataIndex: "price",
    key: "price",
    // @ts-ignore
    render: function renderPrice(price: Array<Object>): JSX.Element {
      return (
        <div className={styles.wrap__price}>
          <Icon type="Bold" name="CurrencyCircleDollar" />
          <div className={styles.price__value}>{price}</div>
        </div>
      );
    },
  },
];

let images = [
  {
    id: 1,
    url: `https://i.guim.co.uk/img/media/684c9d087dab923db1ce4057903f03293b07deac/205_132_1915_1150/master/1915.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=14a95b5026c1567b823629ba35c40aa0`,
  },
  {
    id: 2,
    url: `https://ggsc.s3.amazonaws.com/images/uploads/The_Science-Backed_Benefits_of_Being_a_Dog_Owner.jpg `,
  },
  {
    id: 3,
    url: `https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg`,
  },
  {
    id: 4,
    url: `https://i.guim.co.uk/img/media/684c9d087dab923db1ce4057903f03293b07deac/205_132_1915_1150/master/1915.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=14a95b5026c1567b823629ba35c40aa0`,
  },
  {
    id: 5,
    url: `https://ggsc.s3.amazonaws.com/images/uploads/The_Science-Backed_Benefits_of_Being_a_Dog_Owner.jpg `,
  },
  {
    id: 6,
    url: `https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg`,
  },
  {
    id: 7,
    url: `https://i.guim.co.uk/img/media/684c9d087dab923db1ce4057903f03293b07deac/205_132_1915_1150/master/1915.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=14a95b5026c1567b823629ba35c40aa0`,
  },
  {
    id: 8,
    url: `https://ggsc.s3.amazonaws.com/images/uploads/The_Science-Backed_Benefits_of_Being_a_Dog_Owner.jpg `,
  },
  {
    id: 9,
    url: `https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg  `,
  },
  {
    id: 10,
    url: `https://i.guim.co.uk/img/media/684c9d087dab923db1ce4057903f03293b07deac/205_132_1915_1150/master/1915.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=14a95b5026c1567b823629ba35c40aa0`,
  },
  {
    id: 11,
    url: `https://ggsc.s3.amazonaws.com/images/uploads/The_Science-Backed_Benefits_of_Being_a_Dog_Owner.jpg `,
  },
  {
    id: 12,
    url: `https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg  `,
  },
  {
    id: 13,
    url: `https://ggsc.s3.amazonaws.com/images/uploads/The_Science-Backed_Benefits_of_Being_a_Dog_Owner.jpg `,
  },
  {
    id: 14,
    url: `https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg  `,
  },
  {
    id: 15,
    url: `https://ggsc.s3.amazonaws.com/images/uploads/The_Science-Backed_Benefits_of_Being_a_Dog_Owner.jpg `,
  },
  {
    id: 16,
    url: `https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg`,
  },
];

const AgencyDetail: React.FC = () => {
  const [projectDetail, setProjectDetail] = useState(
    "Ngôi nhà đẹp với toàn bộ mặt tiền bằng gạch này có 5 phòng ngủ, 3 phòng tắm đầy đủ, phòng gia đình, phòng khách trang trọng & phòng ăn trang trọng. Rộng rãi & thoáng mát. Nhà bếp lớn với tầm nhìn mở ra phòng.trọng. Rộng rãi & thoáng mát. Nhà bếp lớn với tầm nhìn mở ra phòng.trọng. Rộng rãi & thoáng mát. Nhà bếp lớn với tầm1 nhìn mở ra phòng. "
  );
  const [isSeeMore, setSeeMore] = useState(false);

  const handleSeeMore = () => {
    setSeeMore(!isSeeMore);
  };
  return (
    <div>
      <Row>
        <Col className={styles.wrap__left} span={12}>
          <div className={styles.main__content_left}>
            <div className={styles.card}>
              <div className={styles.main__left_top}>
                <Carousel images={images} />
                <div className={styles.address}>
                  <span className={styles.address__key}>Địa chỉ:</span>
                  <span className={styles.address__value}>
                    Hoàng Hữ Nam, Phường Long Bình, Q9, Tp HCM.{" "}
                  </span>
                </div>
                <div>
                  <Row>
                    <Col span={12}>
                      <div className={styles.wrap__btn}>
                        <div className={styles.btn__left}>
                          <div className={styles.btn__follow}>
                            <Icon type="Bold" name="PlusCircle" />
                            <div>Theo dõi</div>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.wrap__btn}>
                        <div className={styles.btn__right}>
                          <div className={styles.btn__chat}>
                            <Icon type="Bold" name="ChatsCircle" />
                            <div>Trò chuyện</div>
                          </div>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </div>
            <div className={styles.card}>
              <div className={styles.main__left_bottom}>
                <div className={styles.wrap__left_bottom}>
                  <Image
                    className={styles.logo}
                    src="https://leaderreaderjournal.com/wp-content/uploads/2021/01/dog.jpg"
                    alt="Vercel Logo"
                    width={40}
                    height={40}
                  />
                  <div className={styles.left__info}>
                    <div className={styles.info__name}>Justin Nguyen</div>
                    <div className={styles.info__detail}>
                      Cá Nhân . 25.08.2021
                    </div>
                  </div>
                  <div className={styles.social}>
                    <div className={styles.social__icon}>
                      <Icon type="Bold" name="YoutubeLogo" />
                    </div>
                    <div className={styles.social__icon}>
                      <Icon type="Bold" name="FacebookLogo" />
                    </div>
                    <div className={styles.social__icon}>
                      <Icon type="Bold" name="TelegramLogo" />
                    </div>
                    <div className={styles.social__icon}>
                      <Icon type="Bold" name="Envelope" />
                    </div>
                  </div>
                </div>
                <div className={styles.wrap__right_bottom}>
                  <div className={styles.right__circle}>
                    <Icon type="Bold" name="Phone" />
                  </div>
                  <div className={styles.right__circle}>
                    <Icon type="Bold" name="UploadSimple" />
                  </div>
                  <div className={styles.right__circle}>
                    <Icon type="Bold" name="BookmarkSimple" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Col>
        <Col span={12}>
          <div className={styles.main__content_right}>
            <div
              className={cn(
                styles.card,
                styles.card__protected,
                styles.left__padding
              )}
            >
              <div className={styles.wrap__content}>
                <div className={styles.protected__icon}>
                  <Icon type="Bold" name="ShieldCheck" />
                </div>
                <div className={styles.wrap__content_support}>
                  <div className={styles.support_user}>
                    Hỗ trợ bảo vệ người dùng
                  </div>
                  <div className={styles.detail_support}>
                    Mọi bất động sản và môi giới đều đã được xác thực và kiểm
                    chứng chất lượng bởi hệ thống của chúng tôi để bảo về người
                    dùng.
                  </div>
                </div>
              </div>
            </div>
            <div className={cn(styles.card, styles.left__padding)}>
              <div className={styles.project__type}>
                <div className={styles.project__left}>
                  <div className={styles.project__status_p}>Dự án</div>
                  <div className={styles.project__status_o}>Đang mở bán</div>
                </div>
                <div className={styles.star}>
                  <div>4.5</div>
                  <Icon type="Bold" name="Star" />
                </div>
              </div>
              <div className={styles.title}>The 9 Stellars</div>
              <div className={styles.project_bootom}>
                <div className={styles.project__value}>
                  <div className={styles.items__t1}>
                    <Icon type="Bold" name="CircleWavy" />
                    <div>53%</div>
                  </div>
                  <div className={styles.items__t2}>
                    <Icon type="Bold" name="CurrencyCircleDollar" />
                    <div>127.5 Triệu </div>
                    <div className={styles.unit}>&nbsp;/m2</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={cn(styles.card, styles.left__padding)}>
            <div className={styles.project__info}>Thông tin Dự án</div>
            <div className={styles.project_more_detail}>{projectDetail}</div>
            {isSeeMore ? (
              <div onClick={handleSeeMore} className={styles.seemore}>
                Xem ít
              </div>
            ) : (
              <div onClick={handleSeeMore} className={styles.seemore}>
                Xem thêm
              </div>
            )}
            <div className={styles.line}></div>
            <div className={styles.project_inside}>Trong dự án</div>
            <div className={styles.project_items}>
              <Table
                // @ts-ignore
                columns={columns}
                dataSource={dataSource}
                pagination={false}
                bordered={false}
                _className="agencyDetail"
              />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default AgencyDetail;

import React from 'react'
import Image from 'next/image'
import {SearchBox, Icon} from 'ui-components'
import * as logo from 'public/images/logos/main-logo.svg'


const Header = () => {
    return(
        <div className="mgi-header">
            <div className="logo">
                <Image src={logo} alt="MGI Logo" layout="responsive"/>
            </div>
            <div className="header-form-search">
                <SearchBox placeholder="Tìm BDS, Khu vực, Môi giới..."/>
            </div>
            <div className="header-contact">
                <div className="phone-info">
                   <Icon type="Light" name="Phone"/> +84 0356289
                </div>
            </div>
        </div>
    )
}

export default Header
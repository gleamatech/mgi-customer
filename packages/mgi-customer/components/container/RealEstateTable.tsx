import { Table } from "ui-components";
import { Icon } from "ui-components";

import styles from "../../styles/container/realEstateTable.module.scss";
import * as logo from "public/images/logos/main-logo.svg";
import Image from "next/image";
import RealEstateMobile from './RealEstateMobile'
export interface ObjectInfoRealEstate {
  sold?: number;
  selling?: number;
  like?: number;
  view?: number;
  star?: string;
}

export interface ObjectDataSource {
  key?: string;
  image?: string;
  name?: string;
  description?: string;
  info: ObjectInfoRealEstate;
}

export interface ObjectColumns {
  title?: string;
  dataIndex?: string;
  key?: string;
  render?: JSX.Element;
}

const dataSource: ObjectDataSource[] = [
  {
    image: "",
    key: "0",
    name: "Michael Viani",
    description:
      "Nhiệm vụ của tôi là tạo ra một trải nghiệm bất động sản khác biệt hơn nhiều - một.",
    info: {
      sold: 1248,
      selling: 1248,
      like: 320,
      view: 450,
      star: "4.5",
    },
  },
  {
    image: "",
    key: "1",
    name: "Michael Viani",
    description:
      "Nhiệm vụ của tôi là tạo ra một trải nghiệm bất động sản khác biệt hơn nhiều - một.",
    info: {
      sold: 1248,
      selling: 1248,
      like: 320,
      view: 450,
      star: "4.5",
    },
  },
  {
    image: "",
    key: "2",
    name: "Michael Viani",
    description:
      "Nhiệm vụ của tôi là tạo ra một trải nghiệm bất động sản khác biệt hơn nhiều - một.",
    info: {
      sold: 1248,
      selling: 1248,
      like: 320,
      view: 450,
      star: "4.5",
    },
  },
  {
    image: "",
    key: "3",
    name: "Michael Viani",
    description:
      "Nhiệm vụ của tôi là tạo ra một trải nghiệm bất động sản khác biệt hơn nhiều - một.",
    info: {
      sold: 1248,
      selling: 1248,
      like: 320,
      view: 450,
      star: "4.5",
    },
  },
  {
    image: "",
    key: "4",
    name: "Michael Viani",
    description:
      "Nhiệm vụ của tôi là tạo ra một trải nghiệm bất động sản khác biệt hơn nhiều - một.",
    info: {
      sold: 1248,
      selling: 1248,
      like: 320,
      view: 450,
      star: "4.5",
    },
  },
];

const columns: ObjectColumns[] = [
  {
    title: "Image",
    dataIndex: "iamge",
    key: "image",
    // @ts-ignore
    render: function renderImage(image: string): JSX.Element {
      return (
        <div>
          <Image src={logo} width={32} height={32} alt="main-logo" layout="fixed" />
        </div>
      );
    },
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    // @ts-ignore
    render: function renderName(name: string): JSX.Element {
      return (
        <div className={styles.wrapName}>
          <div className={styles.realEstateName}>{name}</div>
          <div className={styles.timeline}>Cá Nhân</div>
        </div>
      );
    },
  },
  {
    title: "description",
    dataIndex: "description",
    key: "description",
    // @ts-ignore
    render: function renderDescription(description: string): JSX.Element {
      return (
        <div>
          <div className={styles.description}>{description}</div>
        </div>
      );
    },
  },
  {
    title: "info",
    dataIndex: "info",
    key: "info",
    // @ts-ignore
    render: function renderInfo(info): JSX.Element {
      return (<div className={styles.wrapDetail} >
        <div className={styles.detailItem}>
          <div>Đã bán:</div>
          <div>{info.sold}</div>
        </div>
        <div className={styles.detailItem}>
          <div>Đang bán:</div>
          <div>{info.selling}</div>
        </div>
        <div className={styles.detailItem}>
          <div>Yêu thích:</div>
          <div>{info.like}</div>
        </div>
        <div className={styles.detailItem}>
          <div>Đang xem:</div>
          <div>{info.view}</div>
        </div>
        <div className={styles.detailItem}>
          <div>Đánh giá:</div>
          <div>{info.star}</div>
          <Icon type='Bold' name='Star' />
        </div>
      </div >)
    }
  },
];

export default function RealEstateTable() {
  return (
    <div>
      <div className={styles.desktop}>
        <Table
          // @ts-ignore
          columns={columns}
          dataSource={dataSource}
          pagination={false}
          bordered={false}
          _className="realEstateTable"
        />
      </div>
      <div className={styles.mobile}>
        <RealEstateMobile dataSource={dataSource} />
      </div>
    </div>
  );
}

import React from 'react'
import Head from 'next/head'
import Header from './Header'
type layoutProps = {
    children?: React.ReactNode,
    title?: string,
    description?: string,
    keyword?: string,
    isNotPadding?: boolean
}

const Layout = ({children, title, description, keyword, isNotPadding}: layoutProps) => {
    return(
        <React.Fragment>
            <Head>
                <title>{title}</title>
                <meta name="keyword" content={keyword}/>
                <meta name="description" content={description}/>
            </Head>
            <Header/>
            <div className={`main-layout ${isNotPadding && 'main-layout--not-padding'}`}>
                {children}
            </div>
        </React.Fragment>
    )
}

export default Layout;
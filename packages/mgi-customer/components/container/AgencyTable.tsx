/* eslint-disable react/display-name */
import { Table } from "ui-components";
import styles from "../../styles/container/agencyTable.module.scss";
import { Icon } from 'ui-components'
import * as logo from 'public/images/logos/main-logo.svg'
import Image from 'next/image'
import cn from "classnames"
import { ColumnsType } from "antd/es/table";
export interface ObjectDataSource {
  key?: string;
  name?: string;
  description?: string;
  type?: Array<string>;
  price?: string;
  star?: string;
  image?: string;
  address?: string
}
export interface AgencyObject {
  dataIndex?: number
  key?: number
  title?: string
  name?: string
  render?: JSX.Element
}

const dataSource: ObjectDataSource[] = [
  {
    image: '',
    key: "0",
    name: "Mike",
    description:
      "Căn hộ One Veranah tầng trung hoàn thiện cơ bản view thành phố.",
    type: ["Bán", "Thẩm Định"],
    price: "24 tỷ",
    star: "4.5",
    address: 'Hoàng Hữ Nam, Phường Long Bình, Q9, Hồ Chí Minh.',
  },
  {
    image: '',
    key: "1",
    name: "Mike",
    description:
      "Căn hộ One Veranah tầng trung hoàn thiện cơ bản view thành phố.",
    type: ["Bán", "Thẩm Định"],
    price: "24 tỷ",
    star: "4.5",
    address: 'Hoàng Hữ Nam, Phường Long Bình, Q9, Hồ Chí Minh.',
  },
  {
    image: '',
    key: "1",
    name: "Mike",
    description:
      "Căn hộ One Veranah tầng trung hoàn thiện cơ bản view thành phố.",
    type: ["Bán", "Thẩm Định"],
    price: "24 tỷ",
    star: "4.5",
    address: 'Hoàng Hữ Nam, Phường Long Bình, Q9, Hồ Chí Minh.',
  },
  {
    image: '',
    key: "1",
    name: "Mike",
    description:
      "Căn hộ One Veranah tầng trung hoàn thiện cơ bản view thành phố.",
    type: ["Bán", "Thẩm Định"],
    price: "24 tỷ",
    star: "4.5",
    address: 'Hoàng Hữ Nam, Phường Long Bình, Q9, Hồ Chí Minh.',
  },
  {
    image: '',
    key: "1",
    name: "Mike",
    description:
      "Căn hộ One Veranah tầng trung hoàn thiện cơ bản view thành phố.",
    type: ["Bán", "Thẩm Định"],
    price: "24 tỷ",
    star: "4.5",
    address: 'Hoàng Hữ Nam, Phường Long Bình, Q9, Hồ Chí Minh.',
  },
];

const columns: ColumnsType<ObjectDataSource> = [
  {
    title: "Image",
    dataIndex: "iamge",
    key: "image",
    // @ts-ignore
    render: (image: string): JSX.Element => (
      <div className={styles.mainImage}>
        <Image src={logo} alt="main-logo" layout="responsive" />
      </div>
    ),
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    // @ts-ignore
    render: (name: string): JSX.Element => (
      <div className={styles.wrapName}>
        <div className={styles.agencyName}>{name}</div>
        <div className={styles.timeline}>Công ty . 7 phút trước</div>
      </div>
    ),
  },
  {
    title: "description",
    dataIndex: "description",
    key: "description",
    // @ts-ignore
    render: (description: string): JSX.Element => (
      <div>
        <div className={styles.description}>{description}</div>
      </div>
    ),
  },
  {
    title: "type",
    dataIndex: "type",
    key: "type",
    // @ts-ignore
    render: (type: Array<Object>, record): JSX.Element => (
      <div className={styles.typeRecord}>
        <div className={styles.wrapType}>
          {type.map((item, index) => (
            <div key={index} className={cn(styles.type, { [styles.backgroundYellow]: index === 1 })}>{item}</div>
          ))}
        </div>
        <div className={styles.wrapStarMobile}>
          <div className={styles.starValue}>
            {record.star}
          </div>
          <Icon type='Bold' name='Star' />
        </div>
      </div>
    ),
  },
  {
    title: "price",
    dataIndex: "price",
    key: "price",
    // @ts-ignore
    render: (price: Array<Object>): JSX.Element => (
      <div className={styles.wrapPrice}>
        <Icon type='Bold' name='CurrencyCircleDollar' />
        <div className={styles.priceValue}>
          {price}
        </div>
      </div>
    ),
  },
  {
    title: "detail",
    dataIndex: "detail",
    key: "detail",
    // @ts-ignore
    render: (detail): JSX.Element => (
      <div className={styles.wrapDetail}>
        <div className={styles.detailItem}>
          <Icon type='Bold' name='StackSimple' />
          <div>300m2</div>
        </div>
        <div className={styles.detailItem}>
          <Icon type='Bold' name='Bed' />
          <div>6</div>
        </div>
        <div className={styles.detailItem}>
          <Icon type='Bold' name='Drop' />
          <div>4</div>
        </div>
        <div className={styles.detailItem}>
          <Icon type='Bold' name='Compass' />
          <div>Đông Nam</div>
        </div>
      </div>
    ),
  },
  {
    title: "address",
    dataIndex: "address",
    key: "address",
    // @ts-ignore
    render: (address: string): JSX.Element => (
      <div className={styles.address}>
        <div className={styles.addressTitle}>Vị trí:&nbsp;&nbsp;</div><div className={styles.addressValue}> {address}</div>
      </div>
    ),
  },
  {
    title: "star",
    dataIndex: "star",
    key: "star",
    // @ts-ignore
    render: (price: Array<Object>): JSX.Element => (
      <div className={styles.wrapStar}>
        <div className={styles.starValue}>
          {price}
        </div>
        <Icon type='Bold' name='Star' />
      </div>
    ),
  },
];

export default function AgencyTable() {
  return (
    <Table
      // @ts-ignore
      columns={columns}
      dataSource={dataSource}
      pagination={false}
      bordered={false}
      _className="agencyTable"
    />
  );
}

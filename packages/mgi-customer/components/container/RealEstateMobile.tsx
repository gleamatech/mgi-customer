import React from "react";
import styles from "../../styles/container/realEstateTable.module.scss";
import { Icon } from "ui-components";
import Image from 'next/image'

export default function RealEstateMobile(props: any) {
  const { dataSource } = props;
  return (
    <div>
      {dataSource.map((item: any, index: any) => (
        <div key={index} className={styles.card}>
          <div className={styles.left}>
            <Image alt="image-real-estate" width="133" height="133" src="https://tvline.com/wp-content/uploads/2015/07/nathaniel-buzolic-the-originals.jpg?w=620" />
          </div>
          <div className={styles.right}>
            <div className={styles.wrapItem}>
              <div className={styles.high}>Cao cấp</div>
              <div className={styles.realMobileStar}>
                <div>5.0</div>
                <Icon type="Bold" name="Star" />
              </div>
            </div>
            <div className={styles.realMobileName}>{item.name}</div>
            <div className={styles.realMobileQuality}>
              <div className={styles.wrapQuality}>
                <div>Đang bán</div>
                <div>1280</div>
              </div>
              <div className={styles.wrapQuality}>
                <div>Đã bán</div>
                <div>344</div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}

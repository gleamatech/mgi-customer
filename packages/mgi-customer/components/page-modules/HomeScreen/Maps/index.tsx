import Head from 'next/head';
import React, { useEffect, useState } from 'react'

declare global{
  interface Window{
    google: any
  }
}

const Map = () => {
    const  [isReady, setIsReady] = useState(false)

    useEffect(()=> {
        if(typeof global.window !== "undefined"){
            const ApiKey = process.env.NEXT_PUBLIC_GOOGLE_API;
            const script = document.createElement('script');
            script.src = `https://maps.googleapis.com/maps/api/js?key=${ApiKey}`;
            script.async = true;
            script.defer = true;
            script.addEventListener('load', () => {
              setIsReady(true)
            });
        
            document.body.appendChild(script);
        }
    },[])

    useEffect(()=> {
        if(isReady){
            const map = new global.window.google.maps.Map(document.getElementById('map'), {
                center: {lat: 10.851248587713426, lng: 106.69319099419943},
                zoom: 12,
                mapTypeId: 'roadmap',
                mapId:'997d9b66c335a5e0',
                panControl: false,
                zoomControl: false,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                overviewMapControl: false,
                rotateControl: false
            });
        }   
    },[isReady])

  return (
    <div className="map-screen" id="map">some map information
    </div>
  );
};

export default Map;

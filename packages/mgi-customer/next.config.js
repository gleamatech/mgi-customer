// module.exports = (phase, {defaultConfig}) => {
//   if ('sassOptions' in defaultConfig) {
//       defaultConfig['sassOptions'] = {
//           includePaths: ['./src'],
//           prependData: `@import "ui-components/src/styles/main.scss";`,
//       }
//   }

//   defaultConfig.reactStrictMode = true;
//   return defaultConfig;
// }

const withLess = require("next-with-less");

module.exports = withLess({
  reactStrictMode:true,
  publicRuntimeConfig: {
    staticFolder: '/public',
  },
  images: {
    domains: ['www.takadada.com', 'haiau.ttreal.com.vn'],
  }
});
import React from 'react'
// import GridAgency from 'components/page-module/GridAgency'
import GridRealEstate from 'components/page-module/GridRealEstate'



const About = () => {
    return(
        <div className="about-page">
            {/* <GridAgency /> */}
            <GridRealEstate />
        </div>
    )
}

export default About
import React from 'react'
import AgencyTable from '../components/container/AgencyTable';
import RealEstate from '../components/container/RealEstateTable';
import { TabLayout } from 'ui-components'
import Layout from 'components/container/Layout'
import GridRealEstate from 'components/page-module/GridRealEstate'
import GridAgency from 'components/page-module/GridAgency'
import Maps from 'components/page-modules/HomeScreen/Maps'
import { useState } from 'react';




export default function Home() {
  const [typeShow, setTypeShow] = useState('MAP')
  const [typeValue, setTypeValue] = useState('REAL_ESTATE')

  const titleFilter: string[] = ['TP.Hồ Chí Minh', 'Quận 2', 'Đất nền', '>70m2']

  const objData: any = {
    MAP: {
      REAL_ESTATE: <Maps />,
      AGENCY: <Maps />,
    },
    GRID: {
      REAL_ESTATE: <GridRealEstate />,
      AGENCY: <GridAgency />,
    },
    LIST: {
      REAL_ESTATE: <AgencyTable />,
      AGENCY: <RealEstate />,
    }
  }

  const handleSetTypeShow = (type: string) => {
    setTypeShow(type)
  }

  const handleSetTypeValue = (type: string) => {
    setTypeValue(type)
  }



  return (
    <Layout title={`Home Page`}>

      <TabLayout dataTitleFilter={titleFilter} typeShow={typeShow} handleSetTypeShow={handleSetTypeShow} typeValue={typeValue} handleUpdateChangeValue={handleSetTypeValue} >
        {objData[typeShow][typeValue]}

      </TabLayout>
    </Layout>
  )
}

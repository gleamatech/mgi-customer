import type { AppProps } from 'next/app'
import dynamic from 'next/dynamic'
import "styles/config.less"
import "styles/global.scss"
//UI Bundle
import "ui-components/dist/index.css"
import "ui-components/src/styles/global.scss"


function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
export default MyApp

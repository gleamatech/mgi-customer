import React from 'react';
import Image from 'next/image'
import { Button } from 'ui-components'

const arrPartner = [1, 2, 3, 4]
const BoxPartner = () => {
    //render
    const renderBoxAvatar = () => {
        return arrPartner.map(item => {
            return (
                <div key={item} className="box-item-avatar-partner">
                    <div className="box-avatar-partner">
                        <Image className='image-avater-partner' layout='fill' src='https://haiau.ttreal.com.vn/storage/uploads/personals/haiau-image_1598261001.jpeg' />
                    </div> 
                    <div className="name-partner">M.Jack</div>
                </div>
            )
        })
    }
    return (
        <div className='partner-container'>
            <div className="t16 partner-container__name">CỘNG TÁC</div>

            <div className="container-avatar-partner">
                {renderBoxAvatar()}
            </div>
            <div className="box-button">
                <Button>bbb</Button>
            </div>
        </div>
    );
};

export default BoxPartner;
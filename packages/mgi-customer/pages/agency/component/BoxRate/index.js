
import React from 'react'
import { Icon } from 'ui-components'


const renderScaleBar = () => {
    const arr = [1, 2, 3, 4, 5]
    return arr.map(item => {
        const renderWidth = item === 1 ? '80%' :  item === 2 ? '60%' : '0%'
        return (
                <div key={item} className="item-scale-bar">
                    <div style={{width: renderWidth}} className="item-scale-bar__active" />
                </div>
        )
    })
}

const renderItemStar = (item, isActive) => {
    const arrItemStar = new Array(item)
    return arrItemStar.fill('Star').map((item, index) => {
        return (
            <Icon key={index} type='Fill' name={item} className={`icon-rate ${isActive && 'icon-rate__active'}`}/>  
        )
    })
} 

const renderStarRate = () => {
    const arr = [5, 4, 3, 2, 1]
    return arr.map(item => {
    const isActive = item === 5 || item === 4
        return (
                <div key={item} className="box-star-rate">
                    {renderItemStar(item, isActive)}
                </div>
        )
    })
}

const BoxRate = () => {
    return(
        <div className="component-rate-container">
            <div className="box-num-rate">
                <h1 className="box-num-rate__number">4.8</h1>
                <div className="box-num-rate__description small">Trên 5</div>
            </div>
            <div className="scale-bar-container">
            {renderScaleBar()}
            </div>
            <div className="container-star-rate">
                <div className="container-star-rate--auto-width">
                    {renderStarRate()} 
                </div>
            </div>
        </div>
    )
}

export default BoxRate
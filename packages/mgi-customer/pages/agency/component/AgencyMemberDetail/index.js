import React from 'react';
import { AvatarAgency, Button, Icon } from 'ui-components'
import BoxRate from '../BoxRate'
import BoxPartner from '../BoxPartner'



const arrParameters =[{title: 'Lượt xem', value: 1.408},{title: 'Lượt theo dõi', value: 8.800},{title: 'Đang xem', value: 513},{title: 'Yêu thích', value: 400.008}]
const LIST_ICON_CONTACT = ['YoutubeLogo', 'InstagramLogo', 'FacebookLogo', 'TelegramLogo', 'TwitterLogo']
const AgencyMemberDetail = () => {
    //render
    const renderParameters = () => {
        return arrParameters.map((param, index) => (
                <div className="box-parameters" key={index}>
                    <div className="box-parameters__title">{param.title}</div>
                    <div className="box-parameters__number">{param.value}</div>
                </div>
        ))
    }

    const renderIconContact = () => {
        return LIST_ICON_CONTACT.map((name, index) => (
            <div className="item-icon-contact" key={index}>
                <Icon type='Fill' name={name} />
            </div>
    ))
    }
    return (
        <div className="agency-members-container">
        <AvatarAgency />
        <div className="box-button-agency">
            <Button>
                {/* <div className="detail-button">
                    
                </div> */}
                aaaa
            </Button>
            <Button>bbb</Button>
        </div>

        <BoxPartner />

        {renderParameters()}

        <div className="box-icon-contact">
            {renderIconContact()}
        </div>

        <div className="description-agency">
        Nhiệm vụ của tôi là tạo ra một trải nghiệm bất động sản khác biệt hơn nhiều - một trải nghiệm vượt xa mong Nhiệm vụ của tôi là tạo ra một trải nghiệm bất động sản khác biệt hơn nhiều - một trải nghiệm vượt xa mong
        </div>
        <div className="view-more">Xem Thêm</div>

        <div className="number-review small">212 đánh gía từ Khách hàng</div>

       <BoxRate />
    </div>
    );
};

export default AgencyMemberDetail;
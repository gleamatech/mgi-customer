import React from 'react'
import AgencyMemberDetail from './component/AgencyMemberDetail'
import Layout from 'components/container/Layout'
import { TabLayout } from 'ui-components'
import GridRealEstate from 'components/page-module/GridRealEstate'
import GridAgency from 'components/page-module/GridAgency'
import Maps from 'components/page-modules/HomeScreen/Maps'
import { useState } from 'react'
// import AgencyTable from '../components/container/AgencyTable';
// import RealEstate from '../components/container/RealEstateTable';

const dataTitleFilter = ['Đà Lạt', 'Mua/Bán/Thuê', 'Giá Mua Bán', 'Loại Bất Động Sản', 'Khoảng Diện Tích']

const DetailAgency = () => {

    const [typeShow, setTypeShow] = useState('GRID')
    const [typeValue, setTypeValue] = useState('REAL_ESTATE')
  
    const objData = {
      GRID: {
        REAL_ESTATE: <GridRealEstate />,
        AGENCY: <GridRealEstate />,
      }
    }
  
    const handleSetTypeShow = (type) => {
      setTypeShow(type)
    }
  
    const handleSetTypeValue = (type) => {
      setTypeValue(type)
    }
  

    

    return(
       
    <Layout title={`Home Page`} isNotPadding={true}>
        <div className="detail-agency-screen">
            <div className="box-image-background-top" style={{backgroundImage: "url('https://www.takadada.com/wp-content/uploads/2019/07/2-42.jpg')"}}/>
            <div className="container-detail-agency">
                <div className='container-detail-agency__member-detail'>
                <AgencyMemberDetail /> 
                </div>
                <TabLayout 
                    typeShow={typeShow} 
                    handleSetTypeShow={handleSetTypeShow} 
                    typeValue={typeValue} 
                    handleUpdateChangeValue={handleSetTypeValue}
                    typeTable={'detailTable'}
                    dataTitleFilter={dataTitleFilter}
                 >
                    {objData[typeShow][typeValue]}
                </TabLayout>

            </div>
            
         </div> 

     
    </Layout>
    )
}

export default DetailAgency
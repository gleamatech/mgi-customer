import * as React from 'react';
interface AppContextInterface {
    typeShow: string;
    setTypeShow: (type: string) => void;
}
declare type Props = {
    children: React.ReactNode;
};
export declare const ContextApp: () => AppContextInterface;
declare const AppContext: (props: Props) => JSX.Element;
export declare const withContext: (WrappedComponent: React.FC<any>) => ({ ...props }: {
    [x: string]: any;
}) => JSX.Element;
export default AppContext;

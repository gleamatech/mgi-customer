/// <reference types="react" />
export declare const TabLayout: (props: {
    children: import("react").ReactNode;
    tabs?: [] | undefined;
    handleSetTypeShow: (c: string) => void;
    handleUpdateChangeValue: (c: string) => void;
    typeShow?: string | undefined;
    typeValue?: string | undefined;
    typeTable?: string | undefined;
    dataTitleFilter: string[];
}) => JSX.Element;

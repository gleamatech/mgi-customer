import React from 'react';
declare type TabLayoutProps = {
    children: React.ReactNode;
    tabs?: [];
    handleSetTypeShow: (c: string) => void;
    handleUpdateChangeValue: (c: string) => void;
    typeShow?: string;
    typeValue?: string;
    typeTable?: string;
    dataTitleFilter: string[];
};
export declare const SelectedContext: React.Context<{}>;
declare const TabLayout: (props: TabLayoutProps) => JSX.Element;
export default TabLayout;

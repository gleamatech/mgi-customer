declare type CustomItemAgency = {
    props?: any;
};
declare function CustomItemAgency({ ...props }: CustomItemAgency): JSX.Element;
export default CustomItemAgency;

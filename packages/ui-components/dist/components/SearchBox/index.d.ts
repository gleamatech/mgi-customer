import { InputProps } from 'antd';
interface SearchProps extends InputProps {
    containerClass?: string;
    placeholder?: string;
}
export declare const SearchBox: (props: SearchProps) => JSX.Element;
export {};

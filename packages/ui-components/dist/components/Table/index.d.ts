import React from 'react';
declare type TableProps = {
    _className?: JSX.Element | string;
    props?: any;
    children?: React.ReactNode;
};
declare const CustomTable: ({ _className, children, ...props }: TableProps) => JSX.Element;
export default CustomTable;

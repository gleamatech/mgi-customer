declare type IconProp = {
    type: 'Light' | 'Bold' | 'Fill';
    name: string;
    props?: any;
    className?: string;
};
declare const Icon: ({ type, name, className, ...props }: IconProp) => JSX.Element;
export default Icon;

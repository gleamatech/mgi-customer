declare type CustomItemAgency = {
    props?: any;
    item?: any;
};
declare function CustomItemAgency(props: CustomItemAgency): JSX.Element;
export default CustomItemAgency;

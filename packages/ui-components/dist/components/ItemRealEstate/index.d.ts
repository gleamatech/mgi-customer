import React from 'react';
declare type ItemRealEstateProps = {
    className?: JSX.ElementClass;
    props?: any;
    children?: React.ReactNode;
};
declare const CustomItemRealEstate: ({ className, children, ...props }: ItemRealEstateProps) => JSX.Element;
export default CustomItemRealEstate;

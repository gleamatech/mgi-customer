declare type RadioProps = {
    data?: DataType[];
    handleUpdateChangeValue: (c: string) => void;
    typeValue?: string;
};
declare type DataType = {
    title: string;
    value: any;
};
declare const RadioGroup: ({ data, handleUpdateChangeValue, typeValue }: RadioProps) => JSX.Element;
export default RadioGroup;

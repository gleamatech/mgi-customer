declare type TypeSwitch = {
    typeShow?: string;
    handleChange: (e: any) => void;
};
declare const TypeSwitch: ({ handleChange, typeShow }: TypeSwitch) => JSX.Element;
export default TypeSwitch;

declare type BoxFilterProps = {
    handleUpdateChangeValue: (c: string) => void;
    typeValue?: string;
    typeTable?: string;
    dataTitleFilter: string[];
};
declare const BoxFilter: (props: BoxFilterProps) => JSX.Element;
export default BoxFilter;

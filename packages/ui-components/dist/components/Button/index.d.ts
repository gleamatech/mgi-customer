import React from 'react';
declare type ButtonProps = {
    className?: JSX.ElementClass;
    props?: any;
    children?: React.ReactNode;
};
declare const CustomButton: ({ className, children, ...props }: ButtonProps) => JSX.Element;
export default CustomButton;

/// <reference types="react" />
import CustomItemAgency from './components/ItemAgency';
import CustomAvatarAgency from './components/AvatarAgency';
export declare const Button: ({ className, children, ...props }: {
    className?: JSX.ElementClass | undefined;
    props?: any;
    children?: import("react").ReactNode;
}) => JSX.Element;
export declare const Modal: ({}: {}) => JSX.Element;
export declare const ItemRealEstate: ({ className, children, ...props }: {
    className?: JSX.ElementClass | undefined;
    props?: any;
    children?: import("react").ReactNode;
}) => JSX.Element;
export declare const Icon: ({ type, name, className, ...props }: {
    type: "Light" | "Bold" | "Fill";
    name: string;
    props?: any;
    className?: string | undefined;
}) => JSX.Element;
export declare const Table: ({ _className, children, ...props }: {
    _className?: string | JSX.Element | undefined;
    props?: any;
    children?: import("react").ReactNode;
}) => JSX.Element;
export declare const ItemAgency: typeof CustomItemAgency;
export declare const AvatarAgency: typeof CustomAvatarAgency;
export * from './components/layouts';
export * from './components/GoogleMap';
export * from './components/Icon';
export * from './components/SearchBox';

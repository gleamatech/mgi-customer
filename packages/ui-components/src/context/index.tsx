import * as React from 'react'

interface AppContextInterface {
  typeShow: string
  setTypeShow: (type: string) => void
}

type Props = {
  children: React.ReactNode
}

const initialState: AppContextInterface = {
  setTypeShow: (type) => type,
  typeShow: 'MAP'
}

const AppCtx = React.createContext(initialState)

export const ContextApp = () => React.useContext(AppCtx)

const AppContext = (props: Props) => {
  const { children } = props
  const [typeShow, setTypeShow] = React.useState<string>('MAP')
  return (
    <AppCtx.Provider value={{ typeShow, setTypeShow }}>
      {children}
    </AppCtx.Provider>
  )
}

export const withContext = (WrappedComponent: React.FC<any>) => {
  return ({ ...props }) => (
    <AppContext>
      <WrappedComponent {...props} />
    </AppContext>
  )
}

export default AppContext

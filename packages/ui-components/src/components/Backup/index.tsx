import React from 'react'
import styles from './style.module.scss'

type CustomItemAgency = {
  props?: any
}

function CustomItemAgency({ ...props }: CustomItemAgency) {
  return (
    <div {...props} className={styles.itemAgencyContainer}>
      ád
    </div>
  )
}

export default CustomItemAgency

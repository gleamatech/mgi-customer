import React from 'react'
import styles from './style.module..scss'
// import cn from 'classnames'
type AvatarAgencyProps = {
  props?: any
}

function CustomAvatarAgency(props: AvatarAgencyProps) {
  return (
    <div {...props} className={styles.avatarAgencyContainer}>
      <div className={styles.boxAvatar}>
        <img
          className={styles.imgAvatar}
          src='https://haiau.ttreal.com.vn/storage/uploads/personals/haiau-image_1598261001.jpeg'
          alt=''
        />
      </div>
      <h3 className={styles.nameAgency}>Michael Viani</h3>
      <div className={styles.typeAgency}>Cá nhân</div>
    </div>
  )
}

export default CustomAvatarAgency

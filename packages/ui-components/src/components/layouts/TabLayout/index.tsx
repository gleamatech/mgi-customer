import React from 'react'
import Icon from 'components/Icon'
import TypeSwitch from 'components/TypeSwitch'
import BoxFilter from 'components/BoxFilter'
import cn from 'classnames'
import styles from './style.module.scss'
import typo from '../../../styles/_typo.scss'
import { ReactComponent as CloseIcon } from '../../../assets/vector/close.svg'
import MainContextProvider from 'mgi-customer/contexts/mainContext'
import { get } from 'lodash'

const dataTabHome = [
  { title: 'Tất cả', isIconClose: false },
  { title: 'Tìm bán', isIconClose: true },
  { title: 'Tìm mua', isIconClose: true },
  { title: 'Dự Án', isIconClose: true },
  { title: 'Riêng Lẻ', isIconClose: true }
]

const dataDetail = [
  { title: 'Danh sách đang bán', total: '20' },
  { title: 'Danh sách đã1 bán', total: '10' },
  { title: 'Đánh giá', total: '300' },
  { title: 'Dịch vụ', total: '05' }
]

type TabLayoutProps = {
  children: React.ReactNode
  tabs?: []
  handleSetTypeShow: (c: string) => void
  handleUpdateChangeValue: (c: string) => void
  typeShow?: string
  typeValue?: string
  typeTable?: string
  dataTitleFilter: string[]
}

export const SelectedContext = React.createContext({})

const TabLayout = (props: TabLayoutProps) => {
  const {
    children,
    typeShow,
    typeValue,
    typeTable,
    dataTitleFilter,
    handleSetTypeShow,
    handleUpdateChangeValue
  } = props

  const isTableDetail = typeTable === 'detailTable'

  const handleChangeType = (event: any) => {
    const { value } = event.target
    handleSetTypeShow(value)
  }

  const renderTabHome = () => {
    const data: {
      title?: string
      isIconClose?: boolean
      total?: string
    }[] = isTableDetail ? dataDetail : dataTabHome

    return data.slice().map((item, index) => {
      const isActive = index === 0
      return (
        <li
          key={Math.random() + index}
          className={cn(styles.tabItem, { [styles.tabItemActive]: isActive })}
        >
          {get(item, 'title')}{' '}
          {get(item, 'isIconClose') && !isTableDetail && (
            <span className={styles.iconClose}>
              <CloseIcon />
            </span>
          )}
          {isTableDetail && (
            <span className={cn(typo.small, styles.totalItem)}>
              {item.total}
            </span>
          )}
        </li>
      )
    })
  }

  return (
    <div className={styles.tabLayout}>
      <div className={styles.tabContainer}>
        <div className={styles.tabsControl}>
          <ul className={styles.tabList}>
            {renderTabHome()}
            <li className={cn(styles.tabItem, styles.tabAddItem)}>
              <Icon type='Bold' name='Plus' />
            </li>
          </ul>
        </div>
        {!isTableDetail && (
          <div className='tabs-switch-mode'>
            <TypeSwitch typeShow={typeShow} handleChange={handleChangeType} />
          </div>
        )}
      </div>
      <MainContextProvider>
        <div className={styles.tabContent}>
          <BoxFilter
            dataTitleFilter={dataTitleFilter}
            typeTable={typeTable}
            typeValue={typeValue}
            handleUpdateChangeValue={handleUpdateChangeValue}
          />
          {children}
        </div>
      </MainContextProvider>
    </div>
  )
}

export default TabLayout

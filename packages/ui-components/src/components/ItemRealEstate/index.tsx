import React from 'react'
import styles from './style.module.scss'
import cn from 'classnames'
import typo from '../../styles/_typo.scss'
import Icon from 'components/Icon'
// import Image from 'next/image'
import { ReactComponent as IconLogo } from '../../assets/vector/iconLogo.svg'
import { ReactComponent as IconStar } from '../../assets/vector/star.svg'
import { ReactComponent as IconDollar } from '../../assets/vector/iconDollar.svg'

// import iconLogo from 'public/icon-logo.svg'

type ItemRealEstateProps = {
  className?: JSX.ElementClass
  props?: any
  children?: React.ReactNode
}

const arrItemDetail = [
  { value: '300m2', icon: 'StackSimple' },
  { value: '6', icon: 'Bed' },
  { value: '5', icon: 'Drop' },
  { value: 'Đông Nam', icon: 'Compass' },
  { value: 'Đà lạt', icon: 'Compass' }
]

const CustomItemRealEstate = ({
  className,
  children,
  ...props
}: ItemRealEstateProps) => {
  const classNames = cn([className, styles.itemGridComponent])
  return (
    <div {...props} className={classNames}>
      {/* {children} */}
      <div className={styles.boxTopItem}>
        {/* background */}
        <div className={styles.boxImageItem}>
          <img
            className={styles.imageItem}
            src='https://vivutour.vn/wp-content/uploads/2020/09/gem-villa-son-tay.jpg'
            alt=''
          />
        </div>

        <div className={styles.boxCompany}>
          <div className={styles.boxCompanyDetail}>
            {/* <img className={styles.iconCompany} src={IconLogo} alt='' /> */}
            <div className={styles.iconCompany}>
              <IconLogo />
            </div>
            <div className={styles.boxName}>
              <div className={cn(typo.small, styles.name)}>MGI Global 01</div>
              <div className={cn(typo.small, styles.company)}>Công ty</div>
            </div>
          </div>
          <div className={cn(typo.small, styles.postTime)}>Hôm qua</div>
        </div>
      </div>

      <div className={styles.detailItemContainer}>
        <div className={styles.boxTopDetail}>
          <div className={styles.boxStatus}>
            <div className={cn(typo.small, styles.sell)}>Bán</div>
            <div className={cn(typo.small, styles.expertise)}>Thẩm Định</div>
          </div>

          <div className={styles.boxRate}>
            <div className={styles.numRate}>4.5</div>
            {/* <div className={styles.iconRate}> */}
            <IconStar className={styles.iconRate} />
            {/* </div> */}
          </div>
        </div>

        <div className={styles.titleDetail}>
          Căn hộ One Veranah tầng trung hoàn thiện cơ bản view thành phố.
        </div>

        <div className={styles.boxPrice}>
          {/* <div className={styles.iconPrice}> */}
          <IconDollar className={styles.iconPrice} />
          {/* </div> */}

          <div className={styles.price}>24 Tỷ</div>
        </div>

        <div className={styles.detailBottomContainer}>
          {arrItemDetail.map((item, index) => (
            <div className={styles.boxItemDetailBottom} key={index}>
              <div className={styles.iconItemDetail}>
                <Icon type='Light' name={item.icon} />
              </div>

              <div className={cn(typo.small, styles.contentItemDetail)}>
                {item.value}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default CustomItemRealEstate

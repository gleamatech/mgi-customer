import { Button } from 'antd'
import React from 'react'
import styles from './style.module.scss'
import cn from 'classnames'

type ButtonProps = {
  className?: JSX.ElementClass
  props?: any
  children?: React.ReactNode
}

const CustomButton = ({ className, children, ...props }: ButtonProps) => {
  const classNames = cn([className, styles.customizeButton])
  return (
    <Button {...props} className={classNames}>
      {children}
    </Button>
  )
}

export default CustomButton

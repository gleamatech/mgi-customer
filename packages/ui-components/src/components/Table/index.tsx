import { Table } from 'antd'
import React from 'react'
import styles from './style.module.scss'
import cn from 'classnames'

type TableProps = {
  _className?: JSX.Element | string
  props?: any
  children?: React.ReactNode
}

const CustomTable = ({ _className, children, ...props }: TableProps) => {
  const classNames = cn(styles.customizeTable, _className)
  return (
    <Table {...props} className={classNames}>
      {children}
    </Table>
  )
}

export default CustomTable

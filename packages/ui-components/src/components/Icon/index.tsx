import React from 'react'
import cn from 'classnames'
import styles from './style.module.scss'
type IconProp = {
  type: 'Light' | 'Bold' | 'Fill'
  name: string
  props?: any
  className?: string
}

const Icon = ({ type = 'Light', name, className, ...props }: IconProp) => {
  const renderClassName = cn(styles[type], styles[`${type}-${name}`], className)
  return <i className={renderClassName} {...props} />
}

export default Icon

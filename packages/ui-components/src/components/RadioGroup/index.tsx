import React from 'react'
import { Radio } from 'antd'
import styles from './style.module.scss'

type RadioProps = {
  data?: DataType[]
  handleUpdateChangeValue: (c: string) => void
  typeValue?: string
}

type DataType = {
  title: string
  value: any
}

const mocks: DataType[] = [
  {
    title: 'Bất động sản',
    value: 'REAL_ESTATE'
  },
  {
    title: 'Môi giới',
    value: 'AGENCY'
  }
]

const RadioGroup = ({
  data = mocks,
  handleUpdateChangeValue,
  typeValue
}: RadioProps) => {
  const handleUpdateValue = (event: any) => {
    const { value } = event.target
    handleUpdateChangeValue(value)
  }

  return (
    <div className={styles.customRadio}>
      <Radio.Group
        className={styles.radioGroup}
        defaultValue={typeValue}
        buttonStyle='solid'
        onChange={handleUpdateValue}
      >
        {data.map((item: DataType) => (
          <Radio.Button value={item.value} key={item.value}>
            {item.title}
          </Radio.Button>
        ))}
      </Radio.Group>
    </div>
  )
}

export default RadioGroup

import React from 'react'
import { Radio } from 'antd'
import Icon from 'components/Icon'
import styles from './style.module.scss'

type TypeSwitch = {
  typeShow?: string
  handleChange: (e: any) => void
}

const TypeSwitch = ({ handleChange, typeShow }: TypeSwitch) => {
  return (
    <div className={styles.customRadio}>
      <Radio.Group
        className={styles.radioGroup}
        defaultValue={typeShow}
        buttonStyle='solid'
        onChange={handleChange}
      >
        <Radio.Button value='GRID'>
          <Icon type='Light' name='SquaresFour' />
        </Radio.Button>
        <Radio.Button value='MAP'>
          <Icon type='Fill' name='GlobeHemisphereWest' />
        </Radio.Button>
        <Radio.Button value='LIST'>
          <Icon type='Light' name='Rows' />
        </Radio.Button>
      </Radio.Group>
    </div>
  )
}

export default TypeSwitch

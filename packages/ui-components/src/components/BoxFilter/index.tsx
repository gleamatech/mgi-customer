import React from 'react'
import RadioGroup from 'components/RadioGroup'
import { Button } from 'index'
import Icon from 'components/Icon'
import styles from './style.module.scss'

type BoxFilterProps = {
  handleUpdateChangeValue: (c: string) => void
  typeValue?: string
  typeTable?: string
  dataTitleFilter: string[]
}

const BoxFilter = (props: BoxFilterProps) => {
  const { handleUpdateChangeValue, typeValue, typeTable, dataTitleFilter } =
    props
  const isTableDetail = typeTable === 'detailTable'

  const renderLeftFilter = () => {
    if (isTableDetail) {
      return (
        <div className={styles.boxLeftFilter}>
          <div className={styles.titleType}>
            Riêng Lẻ:
            <span>400</span>
          </div>
          <div className={styles.titleType}>
            Dự Án:
            <span>300</span>
          </div>
        </div>
      )
    }

    return (
      <div className={styles.filterSwitch}>
        <RadioGroup
          handleUpdateChangeValue={handleUpdateChangeValue}
          typeValue={typeValue}
        />
      </div>
    )
  }

  const renderTitleFilter = () => {
    return dataTitleFilter.map((item, index) => {
      return <li key={Math.random() + index}>{item}</li>
    })
  }
  return (
    <div className={styles.filterContainer}>
      {renderLeftFilter()}

      <div className={styles.advanceFilterContainer}>
        <div className={styles.advanceFilter}>
          <ul>
            {renderTitleFilter()}
            <li>
              <Icon type='Fill' name='Funnel' />
            </li>
          </ul>
        </div>
        {!isTableDetail && <Button>Thay đổi</Button>}
      </div>
    </div>
  )
}

export default BoxFilter

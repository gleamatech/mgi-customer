import React from 'react'
import styles from './style.module.scss'
import cn from 'classnames'
import { ReactComponent as IconStar } from '../../assets/vector/star.svg'
import typo from '../../styles/_typo.scss'
import { get } from 'lodash'

type CustomItemAgency = {
  props?: any
  item?: any
}

function CustomItemAgency(props: CustomItemAgency) {
  const isRate = get(props, 'item') === 1
  return (
    <div className={styles.itemAgencyContainer}>
      <img
        className={cn(styles.imgAvatar, { [styles.imgAvatarNotRate]: !isRate })}
        src='https://haiau.ttreal.com.vn/storage/uploads/personals/haiau-image_1598261001.jpeg'
        alt=''
      />
      <div className={styles.detailAgencyContainer}>
        {isRate && (
          <div className={styles.boxPosition}>
            <div className={cn(typo.small, styles.positionName)}>Cao cấp</div>
            <div className={styles.boxRate}>
              <div className={styles.numRate}>5.0</div>
              <IconStar />
            </div>
          </div>
        )}

        <div className={styles.nameAgency}>Michael Viani</div>
        <div className={styles.boxOnSale}>
          <div className={cn(typo.small, styles.onSaleName)}>Đang bán</div>
          <div className={cn(typo.small, styles.onSaleNumber)}>1280</div>
        </div>
        <div className={styles.boxSold}>
          <div className={cn(typo.small, styles.soldName)}>Đã bán</div>
          <div className={cn(typo.small, styles.soldNumber)}>344</div>
        </div>
      </div>
    </div>
  )
}

export default CustomItemAgency

// eslint-disable-next-line no-unused-vars
import { Input, InputProps } from 'antd'
import cn from 'classnames'
import React from 'react'
import Icon from '../Icon'
import styles from './style.module.scss'

interface SearchProps extends InputProps {
  containerClass?: string
  placeholder?: string
}

export const SearchBox = (props: SearchProps) => {
  const { containerClass, ...restProps } = props
  const classNames = cn(containerClass)
  return (
    <div className={classNames}>
      <Input
        className={styles.inputStyle}
        {...restProps}
        prefix={
          <div className={styles.searchIcon}>
            <Icon type='Light' name='MagnifyingGlass' />
          </div>
        }
      />
    </div>
  )
}

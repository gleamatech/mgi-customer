import CustomItemRealEstate from './components/ItemRealEstate'
import CustomButton from './components/Button'
import CustomTable from './components/Table'
import CustomModal from './components/Modal'
import CustomIcon from './components/Icon'
import CustomItemAgency from './components/ItemAgency'
import CustomAvatarAgency from './components/AvatarAgency'

export const Button = CustomButton
export const Modal = CustomModal
export const ItemRealEstate = CustomItemRealEstate
export const Icon = CustomIcon
export const Table = CustomTable
export const ItemAgency = CustomItemAgency
export const AvatarAgency = CustomAvatarAgency

export * from './components/layouts'
export * from './components/GoogleMap'
export * from './components/Icon'
export * from './components/SearchBox'

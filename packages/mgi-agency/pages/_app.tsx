import '../styles/globals.css'
import "../styles/config.less"
// import "ui-components/dist/index.css"
import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
export default MyApp
